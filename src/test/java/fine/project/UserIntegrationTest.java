package fine.project;

import fine.project.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
public class UserIntegrationTest {
    @Autowired
    private TestRestTemplate restTemplate;

    private User createdUser;

    @Before
    public void setUp() throws Exception {
        createdUser = restTemplate.postForObject("/users", new User("firstName", "lastName"), User.class);
    }

    @Test
    public void getUser() throws Exception {
        List<User> users = restTemplate.getRestTemplate().exchange("/users", HttpMethod.GET, null, new
                ParameterizedTypeReference<List<User>>() {
                }).getBody();
        Assert.assertFalse(users.isEmpty());
        Assert.assertTrue(users.contains(createdUser));
    }
}
