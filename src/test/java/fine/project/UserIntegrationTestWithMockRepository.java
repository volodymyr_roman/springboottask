package fine.project;

import fine.project.dao.UserRepository;
import fine.project.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserIntegrationTestWithMockRepository {

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private UserRepository userRepository;

    private User createdUser = new User("3232Asasa", "someName", "someSurname");

    @Before
    public void setUp() throws Exception {
        given(userRepository.save(any(User.class))).willReturn(createdUser);

        Page<User> usersPage = mock(Page.class);
        given(usersPage.getContent()).willReturn(Collections.singletonList(createdUser));
        given(userRepository.findAll(any(Pageable.class))).willReturn(usersPage);
    }

    @Test
    public void getUser() throws Exception {
        List<User> users = restTemplate.getRestTemplate().exchange("/users", HttpMethod.GET, null, new
                ParameterizedTypeReference<List<User>>() {
                }).getBody();

        Assert.assertEquals(1, users.size());
        Assert.assertEquals(createdUser, users.get(0));
    }
}
