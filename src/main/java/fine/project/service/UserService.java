package fine.project.service;

import fine.project.model.User;

import java.util.List;

public interface UserService {
    User get(String id);

    User create(User user);

    User update(User user);

    void delete(String id);

    List<User> getAll(int page, int pageSize);
}
